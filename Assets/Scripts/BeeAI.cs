﻿using UnityEngine;
using System.Collections;

public class BeeAI : MonoBehaviour {

	private CharacterController characterController;
	private Vector3 moveDir, terrainSize;

	[SerializeField]
	private float moveSpeed;
	private float timerToRandomizeDirection = 8f;

	void OnEnable () {
		characterController = GetComponent<CharacterController> ();
		StartTimer ();

		terrainSize = GameObject.FindObjectOfType<Terrain> ().terrainData.size;
	}

	void Update() {
		RunTimer ();
		if (timer >= timerToRandomizeDirection) {
			transform.rotation = Quaternion.Euler(Random.Range(-25f, 25f), Random.Range(0f, 360f), 0f);
			timerToRandomizeDirection = Random.Range (0f, 20f);
			StartTimer ();
		}

		if (transform.position.x < 0f || transform.position.x >= terrainSize.x / 2 || transform.position.z <= 0 || transform.position.z >= terrainSize.z / 2) {
			Respawn ();
		}
	}

	void FixedUpdate () {
		Vector3 desiredMove = transform.rotation * Vector3.forward;

		RaycastHit hitInfo;
		Physics.SphereCast(transform.position, characterController.radius, Vector3.down, out hitInfo,
			characterController.height/2f, ~0, QueryTriggerInteraction.Ignore);
		desiredMove = Vector3.ProjectOnPlane(desiredMove, hitInfo.normal).normalized;

		moveDir.x = desiredMove.x*moveSpeed;
		moveDir.z = desiredMove.z*moveSpeed;

		characterController.Move (moveDir * Time.fixedDeltaTime);
	}

	void Respawn() {
		transform.rotation = Quaternion.Euler(Random.Range(-25f, 25f), Random.Range(0f, 360f), 0f);
		transform.position = new Vector3 (Random.Range (-terrainSize.x/2f, terrainSize.x/2f), Random.Range (3f, 30f), Random.Range (-terrainSize.z/2f, terrainSize.z/2f));
	}

	#region Timer
	float timer;
	bool isTiming;

	void StartTimer() {
		isTiming = true;
		timer = 0;
	}

	void RunTimer() {
		if (isTiming)
			timer += Time.deltaTime;
	}

	void StopTimer() {
		isTiming = false;
	}
	#endregion

	#region Properties
	public float TimerToRandomizeDirection {
		get {
			return timerToRandomizeDirection;
		}

		set {
			timerToRandomizeDirection = value;
		}
	}
	#endregion
}
