﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIController : MonoBehaviour {

	[SerializeField]
	Text timerText;

	[SerializeField]
	GameObject instructionsUI;

	[SerializeField]
	GameObject timeUpScreen, youWinScreen;

	[SerializeField]
	float timerForInstructions;

	void Start() {
		StartTimer (true);
	}

	void Update() {
		RunTimer ();
		DisplayTimer ();

		if (timer >= timerForInstructions) {
			if (Input.GetButtonDown ("Submit")) {
				instructionsUI.SetActive (false);
				GameObject.FindGameObjectWithTag ("GameController").GetComponent<GameController> ().StartGameTimer ();
				GameObject.FindGameObjectWithTag ("Player").GetComponent<BeeMovement> ().enabled = true;
			}
		} else {
			GameObject.FindGameObjectWithTag ("Player").GetComponent<BeeMovement> ().enabled = false;
		}
	}

	void DisplayTimer() {
		if (GameObject.FindGameObjectWithTag ("GameController") != null) {
			float timer = Mathf.Floor(GameObject.FindGameObjectWithTag ("GameController").GetComponent<GameController> ().Timer);
			string temp = (Mathf.Floor(timer / 60) + ":" + Mathf.FloorToInt(timer % 60).ToString("D2")).ToString();
			timerText.text = temp;
		}
	}

	#region Timer
	bool isTiming;
	float timer;
	void StartTimer(bool restartTimer) {
		isTiming = true;
		if (restartTimer)
			timer = 0;
	}

	void RunTimer() {
		if (isTiming)
			timer += Time.deltaTime;
	}

	void StopTimer() {
		isTiming = false;
	}
	#endregion

	#region GameOver Screens
	public void TimeUpScreenPopup () {
		timeUpScreen.SetActive (true);
	}

	public void YouWinScreenPopup () {
		youWinScreen.SetActive (true);
	}
	#endregion
}
