﻿using UnityEngine;
using System.Collections;

public class FlowerScript : MonoBehaviour {

	public ParticleSystem emitter;

	[SerializeField]
	float flowerTimer, amountOfPollenAvailable, maxAmountOfPollenAvailable = 50f;
	bool isCollecting, canCollect;

	void Start () {
		amountOfPollenAvailable = maxAmountOfPollenAvailable;
	}

	void Update() {
		if (amountOfPollenAvailable > 0) // if there is pollen left in the flower, allow bees to collect
			canCollect = true;
		else
			canCollect = false;

		if (canCollect)
			Collect ();
		
		if (!canCollect) {
			emitter.gameObject.SetActive(false);
		}
	}

	void FixedUpdate() {
		RunTimer ();
	}

	void OnTriggerStay(Collider other) {
		if (other.tag == "Player" && canCollect) { // if player is in the trigger box and the flower has pollen left
			if (Input.GetButtonDown ("Collect")) {
				StartTimer (true);
				isCollecting = true;
				other.GetComponent<BeeMovement> ().CanMove = false;
			}
		}

		if (other.tag == "Player" && Input.GetButtonUp ("Collect")) {
			StopTimer (true);
			isCollecting = false;
			other.GetComponent<BeeMovement> ().CanMove = true;
		}
	}

	void Collect() {
		if (Input.GetButton ("Collect") && canCollect) {
			if (timer > 0.5f) {
				StartTimer (true);
				GameObject.FindGameObjectWithTag("Player").GetComponent<BeeMovement> ().beeAttributes.amountOfPollenCarrying += 5f;
				amountOfPollenAvailable -= 5f;
			}
		} 
	}

	#region Timer
	bool isTiming;
	float timer;

	void StartTimer(bool resetTimer) {
		isTiming = true;
		if (resetTimer)
			timer = 0;
	}

	void RunTimer() {
		if (isTiming)
			timer += Time.deltaTime;
	}

	void StopTimer(bool resetTimer) {
		isTiming = false;
		if (resetTimer)
			timer = 0;
	}
	#endregion
}
