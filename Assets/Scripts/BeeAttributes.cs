﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class BeeAttributes : System.Object {
	public float amountOfPollenCarrying;

	[SerializeField]
	private float maxAmountOfPollenCanCarry;

	public float MaxAmountOfPollenCanCarry {
		get {
			return maxAmountOfPollenCanCarry;
		}
	}
}
