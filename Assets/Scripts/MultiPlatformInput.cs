﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class MultiPlatformInput {

	bool controllerConnected;

	public bool isControllerConnected {
		get {
			if (Input.GetJoystickNames().Length == 0) { // no controller connected
				return false;
			} else if (Input.GetJoystickNames().Length == 1) {
				return Input.GetJoystickNames () [0].ToString () != ""; // no controller connected
			} else { // controller connected
				return true;
			}
		}
	}

	public bool GetButtonDown (string button) {
		if (Input.GetJoystickNames().Length == 0) { // no controller connected
			return Input.GetButtonDown(button);
		} else if (Input.GetJoystickNames().Length == 1) {
			if (Input.GetJoystickNames()[0].ToString() == "") { // no controller connected
				return Input.GetButtonDown(button);
			} else { // controller connected
				return Input.GetButtonDown("Joystick " + button);
			}
		} else { // controller connected
			return Input.GetButtonDown("Joystick " + button);
		}
	}

	public bool GetButton (string button) {
		if (Input.GetJoystickNames().Length == 0) {
			return Input.GetButton(button);
		} else if (Input.GetJoystickNames().Length == 1) {
			if (Input.GetJoystickNames()[0].ToString() == "") { // no controller connected
				return Input.GetButton(button);
			} else { // controller connected
				return Input.GetButton("Joystick " + button);
			}
		} else {
			return Input.GetButton("Joystick " + button);
		}
	}

	public bool GetButtonUp (string button) {
		if (Input.GetJoystickNames().Length == 0) {
			return Input.GetButtonUp(button);
		} else if (Input.GetJoystickNames().Length == 1) {
			if (Input.GetJoystickNames()[0].ToString() == "") { // no controller connected
				return Input.GetButtonUp(button);
			} else { // controller connected
				return Input.GetButtonUp("Joystick " + button);
			}
		} else {
			return Input.GetButtonUp("Joystick " + button);
		}
	}

	public float GetAxis (string axis) {
		if (Input.GetJoystickNames().Length == 0) {
			return Input.GetAxis(axis);
		} else if (Input.GetJoystickNames().Length == 1) {
			if (Input.GetJoystickNames()[0].ToString() == "") { // no controller connected
				return Input.GetAxis(axis);
			} else { // controller connected
				return Input.GetAxis("Joystick " + axis);
			}
		} else {
			return Input.GetAxis("Joystick " + axis);
		}
	}
}
