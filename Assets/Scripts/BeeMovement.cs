﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BeeMovement : MonoBehaviour {

	public BeeAttributes beeAttributes;

	[SerializeField]
	Camera mainCamera;

	[SerializeField]
	Text pollenTextField;

	[SerializeField]
	AudioSource hoverSound, movementSound;

	[SerializeField]
	float movementSpeed;

	Rigidbody rb;
	[SerializeField] CameraLook cameraLook;
	Vector3 forwardVector;
	bool canMove = true;

	void Start () {
		rb = GetComponent<Rigidbody> ();
		if (!UnityEngine.VR.VRDevice.isPresent) { // if VR Headset isn't connected
			cameraLook = new CameraLook();
			cameraLook.Init(transform, mainCamera.transform);
		}
	}

	void Update() {
		pollenTextField.text = "Pollen: " + beeAttributes.amountOfPollenCarrying;

		if (!UnityEngine.VR.VRDevice.isPresent) { // if VR Headset isn't connected
			cameraLook.LookRotation (transform, mainCamera.transform);
		}
	}

	void FixedUpdate () {
		#region Movement 2
		if (rb.velocity.magnitude < 0.5f) {
			if (!hoverSound.isPlaying)
				hoverSound.Play();
			if (movementSound.isPlaying)
				movementSound.Stop();
		} else {
			if (!movementSound.isPlaying)
				movementSound.Play();
			if (hoverSound.isPlaying)
				hoverSound.Stop();
		}

		if (canMove) {
			Vector3 velocity = rb.velocity;

			int sign = (int)(Input.GetAxis("Forward")/Mathf.Abs(Input.GetAxis("Forward")));
			if (Mathf.Abs(Input.GetAxis("Forward")) > 0) {
				velocity = forwardVector * (sign * movementSpeed);
			} else {
				forwardVector = mainCamera.transform.rotation * Vector3.forward;
				velocity = Vector3.forward*0.01f;
			}

			sign = (int)(Input.GetAxis("Horizontal")/Mathf.Abs(Input.GetAxis("Horizontal")));
			if (Mathf.Abs(Input.GetAxis("Horizontal")) > 0) {
				velocity += (mainCamera.transform.rotation * Vector3.right * sign) * (movementSpeed/2f);
			}
			rb.velocity = velocity;
		}
		#endregion
	}
		
	#region Properties
	public bool CanMove {
		get {
			return canMove;
		}

		set {
			canMove = value;
		}
	}
	#endregion
}
