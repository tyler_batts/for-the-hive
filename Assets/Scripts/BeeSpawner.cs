﻿using UnityEngine;
using System.Collections;

public class BeeSpawner : MonoBehaviour {

	[SerializeField]
	GameObject beePrefab;
	GameObject tempBee;
	GameObject[] bees;

	[SerializeField]
	float amountOfBees, beeSpawnRate;
	int numOfBees;
	Vector3 terrainSize;

	void Start() {
		terrainSize = GameObject.FindObjectOfType<Terrain> ().terrainData.size;
		StartTimer (true);
		bees = new GameObject [(int)amountOfBees];
	}

	void Update() {
		RunTimer ();

		if ((timer > beeSpawnRate) && numOfBees < amountOfBees) {
			StartTimer (true);
			SpawnBees ();
			numOfBees++;
		} else if (numOfBees >= amountOfBees) {
			StopTimer ();
		}
	}

	void SpawnBees() {
		Vector3 temp = new Vector3 (Random.Range (-terrainSize.x/2f, terrainSize.x/2f), Random.Range (0f, 30f), Random.Range (-terrainSize.z/2f, terrainSize.z/2f));
		//Vector3 temp = transform.position + GameObject.Find("PlayerSpawn").transform.position + (Vector3.right + (Vector3.right * Random.Range(1f, 10f)));
		tempBee = (GameObject)Instantiate (beePrefab, temp, Quaternion.identity);
		tempBee.transform.rotation = Quaternion.Euler(Random.Range(-25f, 25f), Random.Range(0f, 360f), 0f);
		tempBee.GetComponent<BeeAI> ().TimerToRandomizeDirection = Random.Range (0f, 20f);
		tempBee.transform.parent = transform;
		bees [numOfBees] = tempBee;
	}

	#region Timer
	bool isTiming;
	float timer;

	void StartTimer(bool resetTimer) {
		isTiming = true;
		if (resetTimer)
			timer = 0;
	}

	void RunTimer() {
		if (isTiming)
			timer += Time.deltaTime;
	}

	void StopTimer() {
		isTiming = false;
	}
	#endregion
}
