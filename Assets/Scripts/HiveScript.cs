﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HiveScript : MonoBehaviour {

	public BeeAttributes attributes;

	[SerializeField]
	Text hivePollenText;

	void OnTriggerEnter(Collider other) {
		if (other.tag == "Player") {
			attributes.amountOfPollenCarrying += other.GetComponent<BeeMovement> ().beeAttributes.amountOfPollenCarrying;
			other.GetComponent<BeeMovement> ().beeAttributes.amountOfPollenCarrying = 0;
		}
	}

	public float PollenInHive {
		get {
			return attributes.amountOfPollenCarrying;
		}
	}
}
