﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

	[SerializeField]
	float maxTime;
	bool overTime;

	[SerializeField]
	GameObject playerBee, playerSpawn;

	[SerializeField]
	GameObject timeUp, youWin;

	void Start() {
		if (playerBee == null)
			playerBee = GameObject.FindGameObjectWithTag ("Player");

		if (playerSpawn == null)
			playerSpawn = GameObject.FindGameObjectWithTag ("PlayerSpawn");
	}

	void Update() {
		if (Input.GetMouseButtonDown(0))
			Application.CaptureScreenshot ("Picture.png", 4);

		if (timer+1f > maxTime) {
			timeUp.GetComponentInParent<UIController>().TimeUpScreenPopup ();
			Time.timeScale = 0;
		}

		if (GameObject.FindGameObjectWithTag ("Hive").GetComponent<HiveScript> ().PollenInHive >= GameObject.FindGameObjectWithTag ("Hive").GetComponent<HiveScript> ().attributes.MaxAmountOfPollenCanCarry) {
			youWin.GetComponentInParent<UIController> ().YouWinScreenPopup ();
			Time.timeScale = 0;
		}
	}

	void FixedUpdate() {
		RunTimer ();
	}

	void ReturnPlayerToHive() {
		playerBee.transform.position = playerSpawn.transform.position;
	}

	#region Timer
	float timer;
	bool isTiming;

	public void StartGameTimer() {
		isTiming = true;
		timer = 0;
	}

	void RunTimer() {
		if (isTiming)
			timer += Time.deltaTime;
	}

	void StopTimer() {
		isTiming = false;
	}
	#endregion

	#region properties
	public bool OverTime {
		get {
			return overTime;
		}
	}

	public float Timer {
		get {
			return maxTime - timer;
		}
	}
	#endregion
}
