# README #

### What is this repository for? ###

This game was built for my first project in the Interactive Studio course taught by Charles Palmer and Lisa Brown at Harrisburg University of Science and Technology. Our task was to build a game using VR and we chose to build a proof of concept of what it would be like to live as a honeybee. The game was built in Unity using the Oculus DK2.

For this project, the team was comprised of myself as the developer, Lewis Ellis the 3D artist, Joseph Royall, the sound designer, and Jecyty McCollum the writer and lead designer. 

### How do I get set up? ###

This is playable without an Oculus headset, but the headset is recommended.
The game executable can be found in the downloads section of this repository!